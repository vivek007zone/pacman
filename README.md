# README #

The Android Multiplayer PacMan Project developed by team **BeCray**,
as a part of SV.CO startup revolution batch 3 coding task.

Clone this project and import to android studio to get started.


* Team Lead
    * **Jithu Kiran K**
    jithukiran608@gmail.com



* Repo owner or admin  (Main developer)
    * **Vivek Av**
    vivekav.96@gmail.com

* Co-Founders
    * **Sreeharsh Manas MM**
      sreeharshmanas@gmail.com
    * **Muhammad Yaseen**
      143yaseen143@gmail.com 