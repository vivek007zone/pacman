package com.becray.pacman;

import android.app.Activity;
import android.util.Log;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import java.util.Random;
import java.util.concurrent.Semaphore;

/**
 * Created by BeCray on 03-09-2016.
 */
public class Ghost {
    Thread ghostThread;
    int SPEED;
    int ghostX,ghostY;  //current positions of this ghost.
    int startX,startY;   //starting positions of this ghost.
    int animX=0,animY=0;  //relative position of this ghost with its starting position for animation purposes
    int gridWidth,gridHeight;
    int maxWidth,maxHeight;
    Activity context;
    Map m;
    Semaphore mutex;
    int id;
    TranslateAnimation temp;
    ImageView ghost;
    SwipeDetector.SwipeTypeEnum ghostDir= SwipeDetector.SwipeTypeEnum.LEFT_TO_RIGHT;
    public Ghost(Activity context,int id, ImageView ghost, Map m, Semaphore mutex, int x, int y){
        this.context=context;
        ghostX=x;
        ghostY=y;
        this.startX=ghostX;
        this.startY=ghostY;
        this.id=id;
        this.ghost=ghost;
        this.gridHeight=MainActivity.gridHeight;
        this.gridWidth=MainActivity.gridWidth;
        this.maxHeight=MainActivity.maxHeight;
        this.maxWidth=MainActivity.maxWidth;
        this.m=m;
        this.mutex=mutex;
        this.SPEED=MainActivity.SPEED;
        Log.e("X,Y",startX+"  "+startY);
    }


    public void moveGhost(int fromX,int fromY,SwipeDetector.SwipeTypeEnum ghostDir){
        try {

            mutex.acquire();
            switch(ghostDir){
                case LEFT_TO_RIGHT:--fromX;break;
                case RIGHT_TO_LEFT:++fromX;break;
                case TOP_TO_BOTTOM:--fromY;break;
                case BOTTOM_TO_TOP:++fromY;break;
            }
            ghostX=fromX;
            ghostY=fromY;
            animX=ghostX-startX;
            animY=ghostY-startY;

            switch (ghostDir){
                case LEFT_TO_RIGHT:{
                    if(m.matrix[ghostX+1][ghostY]!= Map.Object.WALL){
                        ghostX++;
                        m.ghostMatrix[ghostX-1][ghostY]= false;
                        m.ghostMatrix[ghostX][ghostY]= true;
                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                temp=new TranslateAnimation(animX*gridWidth,(animX+1)*gridWidth,gridHeight*animY,gridHeight*animY);
                                temp.setDuration(SPEED);
                                temp.setFillAfter(true);
                                temp.setInterpolator(new LinearInterpolator());
                                ghost.startAnimation(temp);
                                animX++;
                            }
                        });
                    }
                    break;
                }
                case RIGHT_TO_LEFT:{
                    if(m.matrix[ghostX-1][ghostY]!= Map.Object.WALL){
                        ghostX--;
                        m.ghostMatrix[ghostX+1][ghostY]= false;
                        m.ghostMatrix[ghostX][ghostY]= true;
                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                temp=new TranslateAnimation(animX*gridWidth,(animX-1)*gridWidth,gridHeight*animY,gridHeight*animY);
                                temp.setDuration(SPEED);
                                temp.setFillAfter(true);
                                temp.setInterpolator(new LinearInterpolator());
                                ghost.startAnimation(temp);
                                animX--;
                            }
                        });
                    }
                    break;
                }
                case TOP_TO_BOTTOM:{
                    if(m.matrix[ghostX][ghostY+1]!= Map.Object.WALL){
                        ghostY++;
                        m.ghostMatrix[ghostX][ghostY-1]= false;
                        m.ghostMatrix[ghostX][ghostY]= true;
                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                temp=new TranslateAnimation(animX*gridWidth,(animX)*gridWidth,gridHeight*animY,gridHeight*(animY+1));
                                temp.setDuration(SPEED);
                                temp.setFillAfter(true);
                                temp.setInterpolator(new LinearInterpolator());
                                ghost.startAnimation(temp);
                                animY++;
                            }
                        });
                    }
                    break;
                }
                case BOTTOM_TO_TOP: {
                    if (m.matrix[ghostX][ghostY - 1] != Map.Object.WALL) {
                        ghostY--;

                        m.ghostMatrix[ghostX][ghostY+1]= false;
                        m.ghostMatrix[ghostX][ghostY]= true;
                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                temp=new TranslateAnimation(gridWidth*animX,gridWidth*animX,animY*gridHeight,(animY-1)*gridHeight);
                                temp.setDuration(SPEED);
                                temp.setFillAfter(true);
                                temp.setInterpolator(new LinearInterpolator());
                                ghost.startAnimation(temp);
                                animY--;
                            }
                        });
                    }
                    break;
                }
            }

            MainActivity.ghostX[id]=ghostX;
            MainActivity.ghostY[id]=ghostY;
            //Log.e("Ghost POS","X: "+ghostX+"  Y: "+ghostY+"  animX: "+animX+"  animY: "+animY);

            
            for(int k=0;k<MainActivity.NO_OF_CLIENTS+1;k++){
                if(ghostX==MainActivity.pacX[k]&&ghostY==MainActivity.pacY[k]){
                    Log.e("Ghost "+id,"Caught on Pacman "+k);
                    MainActivity.pacmans[k].die();
                    MainActivity.caught[k]=true;
                    
                }
            }
            mutex.release();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void randomGhost(final ImageView ghost){
        ghostThread=new Thread(new Runnable() {
            @Override
            public void run() {
                Random r=new Random();
                while(!MainActivity.STOP_ALL_THREADS){
                    int probability=r.nextInt(100);
                    int moves=r.nextInt(17);
                    boolean HitWallflag=true;
                    int i=0;
                    if(probability<25){
                        MainActivity.ghostDirections[id]= SwipeDetector.SwipeTypeEnum.LEFT_TO_RIGHT;
                    }
                    else if(probability<50){
                        MainActivity.ghostDirections[id]= SwipeDetector.SwipeTypeEnum.RIGHT_TO_LEFT;
                    }
                    else if(probability<75){
                        MainActivity.ghostDirections[id]= SwipeDetector.SwipeTypeEnum.TOP_TO_BOTTOM;
                    }
                    else {
                        MainActivity.ghostDirections[id]= SwipeDetector.SwipeTypeEnum.BOTTOM_TO_TOP;
                    }
                    
                    
                    try {
                        switch (MainActivity.ghostDirections[id]){
                            case RIGHT_TO_LEFT:{
                                while(HitWallflag&&i<moves){
                                    if((m.matrix[ghostX-1][ghostY]!= Map.Object.WALL&&!m.ghostMatrix[ghostX-1][ghostY])){
                                        mutex.acquire(); //Log.e("Ghost acq","mutex");
                                        ghostX--;
                                        i++;

                                        m.ghostMatrix[ghostX+1][ghostY]= false;
                                        m.ghostMatrix[ghostX][ghostY]= true;

                                        MainActivity.ghostX[id]=ghostX;
                                        MainActivity.ghostY[id]=ghostY;

                                        MainActivity.updateAllClientsGhostPositions(id);
                                        
                                        for(int k=0;k<MainActivity.NO_OF_CLIENTS+1;k++){
                                            if(ghostX==MainActivity.pacX[k]&&ghostY==MainActivity.pacY[k]){
                                                Log.e("Ghost "+id,"Caught on Pacman "+k);
                                                MainActivity.caught[k]=true;
                                                MainActivity.pacmans[k].die();
                                                
                                            }
                                        }
                                        mutex.release();//Log.e("Ghost rel","mutex");


                                        context.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                temp=new TranslateAnimation(animX*gridWidth,(animX-1)*gridWidth,gridHeight*animY,gridHeight*animY);
                                                temp.setDuration(SPEED);
                                                temp.setFillAfter(true);
                                                temp.setInterpolator(new LinearInterpolator());
                                                ghost.startAnimation(temp);
                                                animX--;
                                            }
                                        });
                                        try {
                                            Thread.sleep(SPEED);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    else {
                                        HitWallflag=false;
                                    }
                                }
                                break;
                            }
                            case BOTTOM_TO_TOP:{
                                while(HitWallflag&&i<moves){

                                    if ( (m.matrix[ghostX][ghostY - 1] != Map.Object.WALL&&!m.ghostMatrix[ghostX][ghostY-1])) {
                                        mutex.acquire(); //Log.e("Ghost acq","mutex");
                                        ghostY--;
                                        i++;
                                        m.ghostMatrix[ghostX][ghostY+1]= false;
                                        m.ghostMatrix[ghostX][ghostY]= true;

                                        MainActivity.ghostX[id]=ghostX;
                                        MainActivity.ghostY[id]=ghostY;

                                        MainActivity.updateAllClientsGhostPositions(id);
                                        
                                        for(int k=0;k<MainActivity.NO_OF_CLIENTS+1;k++){
                                            if(ghostX==MainActivity.pacX[k]&&ghostY==MainActivity.pacY[k]){
                                                Log.e("Ghost "+id,"Caught on Pacman "+k);
                                                MainActivity.pacmans[k].die();
                                                MainActivity.caught[k]=true;
                                                
                                            }
                                        }
                                        mutex.release();

                                        context.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                temp=new TranslateAnimation(gridWidth*animX,gridWidth*animX,animY*gridHeight,(animY-1)*gridHeight);
                                                temp.setDuration(SPEED);
                                                temp.setFillAfter(true);
                                                temp.setInterpolator(new LinearInterpolator());
                                                ghost.startAnimation(temp);
                                                animY--;
                                            }
                                        });
                                        try {
                                            Thread.sleep(SPEED);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    else {
                                        HitWallflag=false;
                                    }
                                }
                                break;
                            }
                            case LEFT_TO_RIGHT:{
                                while (HitWallflag&&i<moves) {
                                    if ((m.matrix[ghostX + 1][ghostY] != Map.Object.WALL&&!m.ghostMatrix[ghostX+1][ghostY])) {
                                        mutex.acquire(); //Log.e("Ghost acq","mutex");
                                        ghostX++;
                                        i++;
                                        m.ghostMatrix[ghostX-1][ghostY]= false;
                                        m.ghostMatrix[ghostX][ghostY]= true;
                                        MainActivity.ghostX[id]=ghostX;
                                        MainActivity.ghostY[id]=ghostY;

                                        MainActivity.updateAllClientsGhostPositions(id);
                                        
                                        for(int k=0;k<MainActivity.NO_OF_CLIENTS+1;k++){
                                            if(ghostX==MainActivity.pacX[k]&&ghostY==MainActivity.pacY[k]){
                                                Log.e("Ghost "+id,"Caught on Pacman "+k);
                                                MainActivity.pacmans[k].die();
                                                MainActivity.caught[k]=true;
                                                
                                            }
                                        }
                                        mutex.release();

                                        context.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                temp=new TranslateAnimation(animX*gridWidth,(animX+1)*gridWidth,gridHeight*animY,gridHeight*animY);
                                                temp.setDuration(SPEED);
                                                temp.setFillAfter(true);
                                                temp.setInterpolator(new LinearInterpolator());
                                                ghost.startAnimation(temp);
                                                animX++;
                                            }
                                        });
                                        try {
                                            Thread.sleep(SPEED);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    else
                                        HitWallflag=false;
                                }
                                break;   
                            }
                            case TOP_TO_BOTTOM:{
                                while (HitWallflag&&i<moves){

                                    if((m.matrix[ghostX][ghostY+1]!= Map.Object.WALL)&&!m.ghostMatrix[ghostX][ghostY+1]) {
                                        mutex.acquire();
                                        ghostY++;
                                        i++;
                                        m.ghostMatrix[ghostX][ghostY-1]= false;
                                        m.ghostMatrix[ghostX][ghostY]= true;
                                        MainActivity.ghostX[id]=ghostX;
                                        MainActivity.ghostY[id]=ghostY;

                                        MainActivity.updateAllClientsGhostPositions(id);
                                        
                                        for(int k=0;k<MainActivity.NO_OF_CLIENTS+1;k++){
                                            if(ghostX==MainActivity.pacX[k]&&ghostY==MainActivity.pacY[k]){
                                                Log.e("Ghost "+id,"Caught on Pacman "+k);
                                                MainActivity.pacmans[k].die();
                                                MainActivity.caught[k]=true;
                                                
                                            }
                                        }
                                        mutex.release();

                                        context.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                temp=new TranslateAnimation(animX*gridWidth,(animX)*gridWidth,gridHeight*animY,gridHeight*(animY+1));
                                                temp.setDuration(SPEED);
                                                temp.setFillAfter(true);
                                                temp.setInterpolator(new LinearInterpolator());
                                                ghost.startAnimation(temp);
                                                animY++;
                                            }
                                        });
                                        try {
                                            Thread.sleep(SPEED);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                    else
                                        HitWallflag=false;
                                }
                                break;
                            }
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                Log.e("Ghost Thread "+id,"Exit "+ghostX+"  "+ghostY);
            }
        });
        ghostThread.start();
    }





    /*
    public void moveGhostThread(){
        ghostThread=new Thread(new Runnable() {
            @Override
            public void run() {
                while (!MainActivity.caught){
                    if(ghostDir!=null){
                        try {
                            mutex.acquire();
                            ghostDir=MainActivity.ghostDirections[id];

                            switch (ghostDir){
                                case LEFT_TO_RIGHT:{
                                    if(m.matrix[ghostX+1][ghostY]!= Map.Object.WALL){
                                        MainActivity.informHost(ghostDir);
                                        //Log.e("Ghost POS","X: "+ghostX+"  Y: "+ghostY+"  animX: "+animX+"  animY: "+animY);
                                        ghostX++;
                                        m.ghostMatrix[ghostX-1][ghostY]= false;
                                        m.ghostMatrix[ghostX][ghostY]= true;
                                        context.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                temp=new TranslateAnimation(animX*gridWidth,(animX+1)*gridWidth,gridHeight*animY,gridHeight*animY);
                                                temp.setDuration(SPEED);
                                                temp.setFillAfter(true);
                                                temp.setInterpolator(new LinearInterpolator());
                                                ghost.startAnimation(temp);
                                                animX++;
                                            }
                                        });
                                    }
                                    break;
                                }
                                case RIGHT_TO_LEFT:{
                                    if(m.matrix[ghostX-1][ghostY]!= Map.Object.WALL){
                                        MainActivity.informHost(ghostDir);
                                        //Log.e("Ghost POS","X: "+ghostX+"  Y: "+ghostY+"  animX: "+animX+"  animY: "+animY);
                                        ghostX--;
                                        m.ghostMatrix[ghostX+1][ghostY]= false;
                                        m.ghostMatrix[ghostX][ghostY]= true;
                                        context.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                temp=new TranslateAnimation(animX*gridWidth,(animX-1)*gridWidth,gridHeight*animY,gridHeight*animY);
                                                temp.setDuration(SPEED);
                                                temp.setFillAfter(true);
                                                temp.setInterpolator(new LinearInterpolator());
                                                ghost.startAnimation(temp);
                                                animX--;
                                            }
                                        });
                                    }
                                    break;
                                }
                                case TOP_TO_BOTTOM:{
                                    if(m.matrix[ghostX][ghostY+1]!= Map.Object.WALL){
                                        MainActivity.informHost(ghostDir);
                                        //Log.e("Ghost POS","X: "+ghostX+"  Y: "+ghostY+"  animX: "+animX+"  animY: "+animY);
                                        ghostY++;
                                        m.ghostMatrix[ghostX][ghostY-1]= false;
                                        m.ghostMatrix[ghostX][ghostY]= true;
                                        context.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                temp=new TranslateAnimation(animX*gridWidth,(animX)*gridWidth,gridHeight*animY,gridHeight*(animY+1));
                                                temp.setDuration(SPEED);
                                                temp.setFillAfter(true);
                                                temp.setInterpolator(new LinearInterpolator());
                                                ghost.startAnimation(temp);
                                                animY++;
                                            }
                                        });
                                    }
                                    break;
                                }
                                case BOTTOM_TO_TOP: {
                                    if (m.matrix[ghostX][ghostY - 1] != Map.Object.WALL) {
                                        MainActivity.informHost(ghostDir);
                                        //Log.e("Ghost POS","X: "+ghostX+"  Y: "+ghostY+"  animX: "+animX+"  animY: "+animY);
                                        ghostY--;
                                        m.ghostMatrix[ghostX][ghostY+1]= false;
                                        m.ghostMatrix[ghostX][ghostY]= true;
                                        context.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                temp=new TranslateAnimation(gridWidth*animX,gridWidth*animX,animY*gridHeight,(animY-1)*gridHeight);
                                                temp.setDuration(SPEED);
                                                temp.setFillAfter(true);
                                                temp.setInterpolator(new LinearInterpolator());
                                                ghost.startAnimation(temp);
                                                animY--;
                                            }
                                        });
                                    }
                                    break;
                                }
                            }


                            MainActivity.ghostX[id]=ghostX;
                            MainActivity.ghostY[id]=ghostY;

                            if(ghostX==MainActivity.pacX&&ghostY==MainActivity.pacY){
                                Log.e("Ghost "+id,"WON  on L"+MainActivity.pacX+"  "+MainActivity.pacY);
                                MainActivity.caught=true;
                                context.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        MediaPlayer mp=MediaPlayer.create(context,R.raw.pacman_death);
                                        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                            @Override
                                            public void onCompletion(MediaPlayer mediaPlayer) {
                                                MainActivity.lostDialog.show();
                                            }
                                        });
                                        mp.start();
                                    }
                                });
                            }
                            else
                                mutex.release(); //Log.e("Ghost rel","mutex");

                            Thread.sleep(SPEED);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        ghostThread.start();
    }*/
}
