package com.becray.pacman;

import android.content.Context;
import android.net.nsd.NsdServiceInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Yaseen on 04-09-2016.
 */
public class HostListAdapter extends ArrayAdapter {
    ArrayList<NsdServiceInfo> services;
    Context context;
    String CHECK_TAG=NsdHelper.CHECK_TAG;
    public HostListAdapter(Context context, int resource, ArrayList<NsdServiceInfo> services) {
        super(context, resource,services);
        this.context=context;
        this.services=services;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View root=inflater.inflate(R.layout.host_list_layout,null);
        TextView name= (TextView) root.findViewById(R.id.list_host_name);
        TextView ip= (TextView) root.findViewById(R.id.list_host_address);
        NsdServiceInfo now=services.get(position);
        String hostname=now.getServiceName().replace(CHECK_TAG,"");
        name.setText(hostname);
        ip.setText(now.getHost().toString());
        return root;
    }

    @Override
    public int getCount() {
        return services.size();
    }
}
