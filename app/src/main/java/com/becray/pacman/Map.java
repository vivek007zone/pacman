package com.becray.pacman;

import java.util.Random;

/**
 * Created by BeCray on 01-09-2016.
 */
public class Map {
    int ROWS =MainActivity.ROWS, COLUMNS =MainActivity.COLUMNS;
    Object[][] matrix=new Object[COLUMNS][ROWS];
    boolean[][] ghostMatrix=new boolean[COLUMNS][ROWS];
    public Map(){
        Random r=new Random();
        for(int i = 0; i< COLUMNS; i++){
            for(int j = 0; j< ROWS; j++){
                matrix[i][j]=Object.EMPTY;
                ghostMatrix[i][j]=false;
            }
        }



    }
    private void drawBorderWalls(){
        drawHorizontal(Object.WALL,0,0, COLUMNS);
        drawHorizontal(Object.WALL, ROWS -1,0, COLUMNS);
        drawVertical(Object.WALL,0,0, ROWS);
        drawVertical(Object.WALL, COLUMNS -1,0, ROWS);
    }
    public void loadMap1(){

        drawBorderWalls();

        drawHorizontal(Object.WALL,2,2,4);
        drawHorizontal(Object.WALL,2,7,4);
        drawHorizontal(Object.WALL,2,12,4);
        drawHorizontal(Object.WALL,4,5,8);
        drawHorizontal(Object.WALL,7,3,5);
        drawHorizontal(Object.WALL,7,10,5);
        drawHorizontal(Object.WALL,9,6,6);
        drawHorizontal(Object.WALL,13,6,6);
        drawHorizontal(Object.WALL,15,6,6);
        drawHorizontal(Object.WALL,17,3,5);
        drawHorizontal(Object.WALL,17,10,5);
        drawHorizontal(Object.WALL,20,5,8);
        drawHorizontal(Object.WALL,22,2,4);
        drawHorizontal(Object.WALL,22,7,4);
        drawHorizontal(Object.WALL,22,12,4);


        drawVertical(Object.WALL,2,2,4);
        drawVertical(Object.WALL,2,9,7);
        drawVertical(Object.WALL,2,19,4);
        drawVertical(Object.WALL,4,10,5);
        drawVertical(Object.WALL,6,11,3);
        drawVertical(Object.WALL,11,11,3);
        drawVertical(Object.WALL,13,10,5);
        drawVertical(Object.WALL,15,2,4);
        drawVertical(Object.WALL,15,9,7);
        drawVertical(Object.WALL,15,19,4);

        /******FOOD*****/
        drawHorizontal(Object.FOOD,1,1,16);
        drawHorizontal(Object.FOOD,3,3,3);
        drawHorizontal(Object.FOOD,3,8,2);
        drawHorizontal(Object.FOOD,3,12,3);
        drawHorizontal(Object.FOOD,5,5,8);
        drawHorizontal(Object.FOOD,8,7,4);
        drawHorizontal(Object.FOOD,10,5,8);
        drawHorizontal(Object.FOOD,14,5,8);
        drawHorizontal(Object.FOOD,16,7,4);
        drawHorizontal(Object.FOOD,19,5,8);
        drawHorizontal(Object.FOOD,21,3,3);
        drawHorizontal(Object.FOOD,21,8,2);
        drawHorizontal(Object.FOOD,21,12,3);
        drawHorizontal(Object.FOOD,23,1,16);

        drawVertical(Object.FOOD,1,1,23);
        drawVertical(Object.FOOD,3,3,3);
        drawVertical(Object.FOOD,3,11,3);
        drawVertical(Object.FOOD,3,19,3);
        drawVertical(Object.FOOD,5,10,5);
        drawVertical(Object.FOOD,12,10,5);
        drawVertical(Object.FOOD,14,3,3);
        drawVertical(Object.FOOD,14,11,3);
        drawVertical(Object.FOOD,14,19,3);
        drawVertical(Object.FOOD,16,1,23);
        /*
        Random r=new Random();
        int i=0;
        int p,q;
        while(i<15){
            if(r.nextInt(100)>75){
                p=r.nextInt(GRIDS_VERTICAL);
                q=r.nextInt(GRIDS_HORIZONTAL);
                if(matrix[p][q]==Object.EMPTY){
                    matrix[p][q]=Object.MEGAFOOD;
                    i++;
                }
            }
            else {
                p=r.nextInt(GRIDS_VERTICAL);
                q=r.nextInt(GRIDS_HORIZONTAL);
                if(matrix[p][q]==Object.EMPTY){
                    matrix[p][q]=Object.FOOD;
                    i++;
                }
            }
        }
*/
    }
    private void drawHorizontal(Object object,int row,int start,int len){
        for(int i=0;i<len;i++){
            matrix[start+i][row]=object;
        }
    }
    private void drawVertical(Object object,int col,int start,int len){
        for(int i=0;i<len;i++){
            matrix[col][start+i]=object;
        }
    }

    public enum Object{
        EMPTY,WALL,FOOD, ENERGIZER
    }
}
