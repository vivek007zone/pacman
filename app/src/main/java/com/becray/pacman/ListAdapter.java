package com.becray.pacman;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by BeCray on 04-09-2016.
 */
public class ListAdapter extends ArrayAdapter {
    Activity context;
    ArrayList<String> names;
    ArrayList<Integer> chars;
    public ListAdapter(Activity context, int resource, ArrayList<String> names, ArrayList<Integer> chars) {
        super(context, resource,names);
        this.context=context;
        this.names=names;
        this.chars=chars;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View root=inflater.inflate(R.layout.connected_clients_list,null);
        TextView name= (TextView) root.findViewById(R.id.list_ghost_name);
        ImageView pic= (ImageView) root.findViewById(R.id.list_ghost_view);

        name.setText(names.get(position));
        switch (chars.get(position)){
            case 0:pic.setImageResource(R.drawable.pacman_yellow_right);break;
            case 1:pic.setImageResource(R.drawable.pacman_blue_right);break;
            case 2:pic.setImageResource(R.drawable.pacman_pink_right);break;
            case 3:pic.setImageResource(R.drawable.pacman_green_right);break;
        }
        return root;
    }

    @Override
    public int getCount() {
        return names.size();
    }
}
