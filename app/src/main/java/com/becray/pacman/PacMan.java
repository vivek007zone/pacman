package com.becray.pacman;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import java.util.concurrent.Semaphore;

/**
 * Created by BeCray on 03-09-2016.
 */
public class PacMan {
    int SPEED;
    boolean energized=false;
    Thread movePacmanThread;
    int gridWidth,gridHeight;
    int maxWidth,maxHeight;
    SwipeDetector.SwipeTypeEnum dir= SwipeDetector.SwipeTypeEnum.TOP_TO_BOTTOM;
    Activity context;
    Map m;
    Semaphore mutex;
    int pacX=1,pacY=1;
    int animX=0,animY=0;
    int startX,startY;
    TranslateAnimation temp;
    ImageView pacman;
    int id;
    int avatar;
    int picUP,picDOWN,picLEFT,picRIGHT;
    int pacmanChomp,pacmanDie,pacmanWin;
    int score=0;
    SoundPool sp;
    public PacMan(Activity context,int id,int avatar,ImageView pacman, Map m, Semaphore mutex){
        this.context=context;
        this.id=id;
        this.avatar=avatar;
        this.pacman=pacman;
        this.gridHeight=MainActivity.gridHeight;
        this.gridWidth=MainActivity.gridWidth;
        this.maxHeight=MainActivity.maxHeight;
        this.maxWidth=MainActivity.maxWidth;
        this.m=m;
        this.mutex=mutex;
        this.SPEED=MainActivity.SPEED;
        startX=pacX;
        startY=pacY;
        sp = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        context.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        pacmanChomp=sp.load(context,R.raw.pacman_chomp,1);
        pacmanDie=sp.load(context,R.raw.pacman_death,1);
        pacmanWin=sp.load(context,R.raw.pacman_intermission,1);

        switch (avatar){
            case 0:
                picUP=R.drawable.pacman_yellow_up;
                picDOWN=R.drawable.pacman_yellow_down;
                picLEFT=R.drawable.pacman_yellow_left;
                picRIGHT=R.drawable.pacman_yellow_right;
                break;
            case 1:
                picUP=R.drawable.pacman_blue_up;
                picDOWN=R.drawable.pacman_blue_down;
                picLEFT=R.drawable.pacman_blue_left;
                picRIGHT=R.drawable.pacman_blue_right;
                break;
            case 2:
                picUP=R.drawable.pacman_pink_up;
                picDOWN=R.drawable.pacman_pink_down;
                picLEFT=R.drawable.pacman_pink_left;
                picRIGHT=R.drawable.pacman_pink_right;
                break;
            case 3:
                picUP=R.drawable.pacman_green_up;
                picDOWN=R.drawable.pacman_green_down;
                picLEFT=R.drawable.pacman_green_left;
                picRIGHT=R.drawable.pacman_green_right;
                break;
        }
    }

    public void movePacmanThread() {

        movePacmanThread=new Thread(new Runnable() {
            @Override
            public void run() {

                while(!MainActivity.STOP_ALL_THREADS&&!MainActivity.caught[id]){
                    if(dir!=null){
                        try {
                            mutex.acquire();
                            dir=MainActivity.pacDirection[id];
                            switch (dir){
                                case LEFT_TO_RIGHT:{
                                    if(m.matrix[pacX+1][pacY]!= Map.Object.WALL){
                //                        Log.e("POS","X: "+pacX+"  Y: "+pacY+"  animX: "+animX+"  animY: "+animY);
                                        if(id==0){
                                            //callee host
                                            MainActivity.updateAllClientsPacmanPosition(0, dir);
                                        }
                                        else {
                                            //callee client
                                            MainActivity.informHost(dir);
                                        }
                                        pacX++;
                                        context.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                pacman.setImageResource(picRIGHT);
                                                temp=new TranslateAnimation(animX*gridWidth,(animX+1)*gridWidth,gridHeight*animY,gridHeight*animY);
                                                temp.setDuration(SPEED);
                                                temp.setFillAfter(true);
                                                temp.setInterpolator(new LinearInterpolator());
                                                pacman.startAnimation(temp);
                                                animX++;
                                            }
                                        });
                                    }
                                    break;
                                }
                                case RIGHT_TO_LEFT:{
                                    if(m.matrix[pacX-1][pacY]!= Map.Object.WALL){
                                        //Log.e("POS","X: "+pacX+"  Y: "+pacY+"  animX: "+animX+"  animY: "+animY);
                                        if(id==0){
                                            //callee host
                                            MainActivity.updateAllClientsPacmanPosition(0,dir);
                                        }
                                        else {
                                            //callee client
                                            MainActivity.informHost(dir);
                                        }
                                        pacX--;
                                        context.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                pacman.setImageResource(picLEFT);
                                                temp=new TranslateAnimation(animX*gridWidth,(animX-1)*gridWidth,gridHeight*animY,gridHeight*animY);
                                                temp.setDuration(SPEED);
                                                temp.setFillAfter(true);
                                                temp.setInterpolator(new LinearInterpolator());
                                                pacman.startAnimation(temp);
                                                animX--;

                                            }
                                        });
                                    }
                                    break;
                                }
                                case TOP_TO_BOTTOM:{
                                    if(m.matrix[pacX][pacY+1]!= Map.Object.WALL){
                                        //Log.e("POS","X: "+pacX+"  Y: "+pacY+"  animX: "+animX+"  animY: "+animY);
                                        if(id==0){
                                            //callee host
                                            MainActivity.updateAllClientsPacmanPosition(0, dir);
                                        }
                                        else {
                                            //callee client
                                            MainActivity.informHost(dir);
                                        }
                                        pacY++;
                                        context.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                pacman.setImageResource(picDOWN);
                                                temp=new TranslateAnimation(gridWidth*animX,gridWidth*animX,animY*gridHeight,(animY+1)*gridHeight);
                                                temp.setDuration(SPEED);
                                                temp.setFillAfter(true);
                                                temp.setInterpolator(new LinearInterpolator());
                                                pacman.startAnimation(temp);
                                                animY++;

                                            }
                                        });
                                    }
                                    break;
                                }
                                case BOTTOM_TO_TOP: {
                                    //Log.e("POS","X: "+pacX+"  Y: "+pacY+"  animX: "+animX+"  animY: "+animY);
                                    if (m.matrix[pacX][pacY - 1] != Map.Object.WALL) {
                                        if(id==0){
                                            //callee host
                                            MainActivity.updateAllClientsPacmanPosition(0, dir);
                                        }
                                        else {
                                            //callee client
                                            MainActivity.informHost(dir);
                                        }
                                        pacY--;
                                        context.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                pacman.setImageResource(picUP);
                                                temp=new TranslateAnimation(gridWidth*animX,gridWidth*animX,animY*gridHeight,(animY-1)*gridHeight);
                                                temp.setDuration(SPEED);
                                                temp.setFillAfter(true);
                                                temp.setInterpolator(new LinearInterpolator());
                                                pacman.startAnimation(temp);
                                                animY--;
                                            }
                                        });

                                    }
                                    break;
                                }
                            }
                            MainActivity.pacX[id]=pacX;
                            MainActivity.pacY[id]=pacY;

                            mutex.release();
                            if(m.ghostMatrix[pacX][pacY]) {
                                Log.e("Lost :", "You were caught by a ghost "+pacX+"  "+pacY);
                                MainActivity.caught[id]=true;
                                die();
                                sp.play(pacmanDie,1,1,1,0,1);
                                break;
                            }
                                 //Log.e("pac rel","mutex");

                            if(m.matrix[pacX][pacY]== Map.Object.FOOD){
                                score++;
                                m.matrix[pacX][pacY]= Map.Object.EMPTY;
                                //MediaPlayer.create(context,R.raw.pacman_chomp).start();

                                sp.play(pacmanChomp,1,1,1,0,1);
                                boolean flag=false;
                                if(checkWin()){
                                    mutex.acquire(); //Log.e("pac acq","mutex");
                                    flag=true;
                                }
                                UpdateMap(pacX,pacY, Map.Object.EMPTY);
                                if(flag){
                                    sp.play(pacmanWin,1,1,1,0,1);

                                    MainActivity.scores[id]=score;
                                    MainActivity.lostDialog.setContent("Score : "+score);
                                    SharedPreferences preferences=context.getSharedPreferences("SETTINGS", Context.MODE_PRIVATE);
                                    preferences.edit().putInt("LAST_SCORE",score).apply();
                                    int total=preferences.getInt("TOTAL_SCORE",0);
                                    total=total+score;
                                    preferences.edit().putInt("TOTAL_SCORE",total).apply();

                                    MainActivity.lostDialog.show();
                                }
                                                            }
                            Thread.sleep(SPEED);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            Log.e("error",e.toString());
                        }
                    }

                }
                if(MainActivity.caught[id]){
                    context.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            sp.play(pacmanDie,1,1,1,0,1);
                            MainActivity.lostDialog.show();
                        }
                    });
                }

                Log.e("pac Thread","Exit");
            }
        });
        movePacmanThread.start();
    }

    public void movePacman(int fromX,int fromY,SwipeDetector.SwipeTypeEnum dir){
        try {
            mutex.acquire();
            switch(dir){
                case LEFT_TO_RIGHT:--fromX;break;
                case RIGHT_TO_LEFT:++fromX;break;
                case TOP_TO_BOTTOM:--fromY;break;
                case BOTTOM_TO_TOP:++fromY;break;
            }
            pacX=fromX;
            pacY=fromY;
            animX=pacX-startX;
            animY=pacY-startY;

            //Log.e("POS","X: "+pacX+"  Y: "+pacY+"  animX: "+animX+"  animY: "+animY);
            switch (dir){
                case LEFT_TO_RIGHT:{
                    if(m.matrix[pacX+1][pacY]!= Map.Object.WALL){
                        pacX++;

                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pacman.setImageResource(picRIGHT);
                                temp=new TranslateAnimation(animX*gridWidth,(animX+1)*gridWidth,gridHeight*animY,gridHeight*animY);
                                temp.setDuration(SPEED);
                                temp.setFillAfter(true);
                                temp.setInterpolator(new LinearInterpolator());
                                pacman.startAnimation(temp);
                                animX++;
                            }
                        });
                    }
                    break;
                }
                case RIGHT_TO_LEFT:{
                    if(m.matrix[pacX-1][pacY]!= Map.Object.WALL){
                        pacX--;

                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pacman.setImageResource(picLEFT);
                                temp=new TranslateAnimation(animX*gridWidth,(animX-1)*gridWidth,gridHeight*animY,gridHeight*animY);
                                temp.setDuration(SPEED);
                                temp.setFillAfter(true);
                                temp.setInterpolator(new LinearInterpolator());
                                pacman.startAnimation(temp);
                                animX--;

                            }
                        });
                    }
                    break;
                }
                case TOP_TO_BOTTOM:{
                    if(m.matrix[pacX][pacY+1]!= Map.Object.WALL){
                        pacY++;

                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pacman.setImageResource(picDOWN);
                                temp=new TranslateAnimation(gridWidth*animX,gridWidth*animX,animY*gridHeight,(animY+1)*gridHeight);
                                temp.setDuration(SPEED);
                                temp.setFillAfter(true);
                                temp.setInterpolator(new LinearInterpolator());
                                pacman.startAnimation(temp);
                                animY++;

                            }
                        });
                    }
                    break;
                }
                case BOTTOM_TO_TOP: {
                    if (m.matrix[pacX][pacY - 1] != Map.Object.WALL) {
                        pacY--;

                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pacman.setImageResource(picUP);
                                temp=new TranslateAnimation(gridWidth*animX,gridWidth*animX,animY*gridHeight,(animY-1)*gridHeight);
                                temp.setDuration(SPEED);
                                temp.setFillAfter(true);
                                temp.setInterpolator(new LinearInterpolator());
                                pacman.startAnimation(temp);
                                animY--;
                            }
                        });
                    }
                    break;
                }
            }
            MainActivity.pacX[id]=pacX;
            MainActivity.pacY[id]=pacY;

            if(m.ghostMatrix[pacX][pacY]) {
                MainActivity.caught[id]=true;
                die();
            }

            mutex.release(); //Log.e("pac rel","mutex");

            if(m.matrix[pacX][pacY]== Map.Object.FOOD){
                score++;
                m.matrix[pacX][pacY]= Map.Object.EMPTY;
                //MediaPlayer.create(context,R.raw.pacman_chomp).start();
                if(checkWin()){
                    mutex.acquire(); //Log.e("pac acq","mutex");
                }
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        UpdateMap(pacX,pacY, Map.Object.EMPTY);
                    }
                });

            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            Log.e("error",e.toString());
        }
    }

    private void UpdateMap(final int i, final int j, Map.Object object){
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MainActivity.grid[i][j].setImageDrawable(null);
            }
        });
    }

    private boolean checkWin(){
        boolean flag=true;
        for(int i = 0; i<MainActivity.COLUMNS; i++){
            for(int j = 0; j<MainActivity.ROWS; j++){
                if(m.matrix[i][j]== Map.Object.FOOD||m.matrix[i][j]== Map.Object.ENERGIZER){
                    flag=false;
                    break;
                }
            }
        }
        MainActivity.caught[id]=flag;
        return flag;
    }

    public void die(){
        MainActivity.scores[id]=score;
        MainActivity.lostDialog.setContent("Score : "+score);
        SharedPreferences preferences=context.getSharedPreferences("SETTINGS", Context.MODE_PRIVATE);
        preferences.edit().putInt("LAST_SCORE",score).apply();
        int total=preferences.getInt("TOTAL_SCORE",0);
        total=total+score;
        preferences.edit().putInt("TOTAL_SCORE",total).apply();
        MainActivity.caught[id]=true;
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                YoYo.with(Techniques.FadeOutDown).duration(1000).withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        pacman.setVisibility(View.GONE);
                        MainActivity.pacX[id]=MainActivity.pacY[id]=pacX=pacY=0;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).playOn(pacman);
            }
        });

    }
}
