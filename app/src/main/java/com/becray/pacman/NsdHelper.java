package com.becray.pacman;

import android.app.Activity;
import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.util.Log;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

/**
 * Created by BeCray on 04-09-2016.
 */

public class NsdHelper {

    Activity mContext;

    NsdManager mNsdManager;
    NsdManager.ResolveListener mResolveListener;
    NsdManager.DiscoveryListener mDiscoveryListener;
    NsdManager.RegistrationListener mRegistrationListener;

    public static final String SERVICE_TYPE = "_http._tcp.";
    public static String CHECK_TAG="69PaC69";
    public static final String TAG = "NsdHelper";
    public String mServiceName = "PacMan";

    NsdServiceInfo mService;

    public NsdHelper(Activity context) {
        mContext = context;
        mNsdManager = (NsdManager) context.getSystemService(Context.NSD_SERVICE);
    }

    public void initializeNsd() {
        //initializeResolveListener(connectedListAdapter, names);

        //mNsdManager.init(mContext.getMainLooper(), this);

    }

    public void initializeDiscoveryListener(final HostListAdapter adapter, final ArrayList<NsdServiceInfo> names) {
        mResolveListener=null;
        initializeResolveListener(adapter, names);
        mDiscoveryListener = new NsdManager.DiscoveryListener() {

            @Override
            public void onDiscoveryStarted(String regType) {
                Log.d(TAG, "Service discovery started");
            }

            @Override
            public void onServiceFound(NsdServiceInfo service) {
                Log.d(TAG, "Service discovery success" + service);
                if (!service.getServiceType().equals(SERVICE_TYPE)) {
                    Log.d(TAG, "Unknown Service Type: " + service.getServiceType());
                } else if (service.getServiceName().contains(CHECK_TAG)){
                    //mNsdManager.stopServiceDiscovery(this);
                    mNsdManager.resolveService(service, initializeResolveListener(adapter,names));
                }
            }

            @Override
            public void onServiceLost(NsdServiceInfo service) {
                Log.e(TAG, "service lost " + service);
                if (mService == service) {
                    mService = null;
                }
                for(NsdServiceInfo nsdServiceInfo : names){
                    if(nsdServiceInfo.getServiceName().equals(service.getServiceName())){
                        names.remove(nsdServiceInfo);
                        mContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyDataSetChanged();
                            }
                        });
                        break;
                    }
                }

            }

            @Override
            public void onDiscoveryStopped(String serviceType) {
                Log.i(TAG, "Discovery stopped: " + serviceType);
            }

            @Override
            public void onStartDiscoveryFailed(String serviceType, int errorCode) {
                Log.e(TAG, "Discovery failed: Error code:" + errorCode);
            }

            @Override
            public void onStopDiscoveryFailed(String serviceType, int errorCode) {
                Log.e(TAG, "Discovery failed: Error code:" + errorCode);
            }
        };
    }

    public NsdManager.ResolveListener initializeResolveListener(final HostListAdapter adapter, final ArrayList<NsdServiceInfo> names) {
        mResolveListener = new NsdManager.ResolveListener() {

            @Override
            public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
                Log.e(TAG, "Resolve failed" + errorCode);
            }

            @Override
            public void onServiceResolved(NsdServiceInfo serviceInfo) {
                Log.e(TAG, "Resolve Succeeded. " + serviceInfo);


                /*if (serviceInfo.getServiceName().equals(mServiceName)) {
                    Log.d(TAG, "Same IP.");
                    return;
                }*/
                boolean flag=true;
                for(NsdServiceInfo service : names){
                    if(service.getHost().equals(serviceInfo.getHost())&&service.getPort()==serviceInfo.getPort()){
                        flag=false;
                        break;
                    }
                }
                if(flag){
                    names.add(serviceInfo);
                    mContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                }
                mService = serviceInfo;
            }
        };
        return mResolveListener;
    }

    public void initializeRegistrationListener(final MaterialDialog hostWaiting) {
        Log.d(TAG, "Reg Lis Init");
        mRegistrationListener = new NsdManager.RegistrationListener() {

            @Override
            public void onServiceRegistered(NsdServiceInfo serviceInfo) {
                mServiceName = serviceInfo.getServiceName();
                Log.d(TAG, "Service registered: " + mServiceName);
                mContext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mContext.getApplicationContext(),"Game Hosted",Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onRegistrationFailed(NsdServiceInfo arg0, int arg1) {
                mRegistrationListener=null;
                Log.d(TAG, "Service registration failed: " + arg1);
                mContext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mContext.getApplicationContext(),"Hosting Failed.Try Again.",Toast.LENGTH_LONG).show();
                        hostWaiting.dismiss();
                    }
                });

            }

            @Override
            public void onServiceUnregistered(NsdServiceInfo arg0) {
                Log.d(TAG, "Service unregistered: " + arg0.getServiceName());
            }

            @Override
            public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
                Log.d(TAG, "Service unregistration failed: " + errorCode);
            }

        };
    }

    public void registerService(String hostName, int port, MaterialDialog hostWaiting) {
        tearDown();  // Cancel any previous registration request
        initializeRegistrationListener(hostWaiting);
        hostName=hostName+CHECK_TAG;
        mServiceName=hostName;
        NsdServiceInfo serviceInfo  = new NsdServiceInfo();
        serviceInfo.setPort(port);
        serviceInfo.setServiceName(mServiceName);
        serviceInfo.setServiceType(SERVICE_TYPE);

        mNsdManager.registerService(
                serviceInfo, NsdManager.PROTOCOL_DNS_SD, mRegistrationListener);

    }

    public void discoverServices(HostListAdapter adapter, ArrayList<NsdServiceInfo> names) {
        stopDiscovery();  // Cancel any existing discovery request
        initializeDiscoveryListener(adapter,names);
        mNsdManager.discoverServices(
                SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, mDiscoveryListener);
    }

    public void stopDiscovery() {
        if (mDiscoveryListener != null) {
            try {
                mNsdManager.stopServiceDiscovery(mDiscoveryListener);
            } finally {
            }
            mDiscoveryListener = null;
        }
    }

    public NsdServiceInfo getChosenServiceInfo() {
        return mService;
    }

    public void tearDown() {
        if (mRegistrationListener != null) {
            try {
                mNsdManager.unregisterService(mRegistrationListener);
            } finally {
            }
            mRegistrationListener = null;
        }
    }
}
