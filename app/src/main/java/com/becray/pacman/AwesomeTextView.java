package com.becray.pacman;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by BeCray on 12-09-2016.
 */
public class AwesomeTextView extends TextView {
    public AwesomeTextView(Context context) {
        super(context);
    }
    public AwesomeTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public AwesomeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init(){
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "astron_boy.ttf");
        setTypeface(tf ,1);
    }
}
