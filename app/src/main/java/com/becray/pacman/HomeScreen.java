package com.becray.pacman;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.nsd.NsdServiceInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.twotoasters.jazzylistview.JazzyHelper;
import com.twotoasters.jazzylistview.JazzyListView;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class HomeScreen extends AppCompatActivity {
    boolean connFull=false;
    NsdHelper helper;
    MaterialDialog startGameDialog,searchingGames,hostWaiting,enterNickName,enterHostName,waitingForGame;
    MaterialDialog settingsDialog,helpDialog;
    static ArrayList<Socket> clients=new ArrayList<>();
    static Socket clientSocket;
    static ServerSocket welcomeSocket;
    int clientId;
    static int[] avatar=new int[4];

    static ArrayList<String> connectedNames;
    ArrayList<Integer> chars;
    ArrayList<NsdServiceInfo> hosts;
    ArrayList<Integer> selectedPacmanChars =new ArrayList<>();
    ListAdapter connectedListAdapter;
    HostListAdapter hostListAdapter;
    JazzyListView hostList,connectedDevicesList;
    String nickName;
    static String hostName;
    SharedPreferences preferences;
    SharedPreferences.Editor prefsEditor;
    boolean soundOn, vibrateOn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //GO FULL SCREEN
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_home_screen);
        getSupportActionBar().hide();


        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        SoundPool sp = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        int pacmanBeginning=sp.load(this,R.raw.pacman_beginning,1);


        helper=new NsdHelper(HomeScreen.this);
        preferences=getSharedPreferences("SETTINGS",MODE_PRIVATE);
        prefsEditor=preferences.edit();

        nickName=preferences.getString("NICKNAME","");
        soundOn =preferences.getBoolean("SOUND",true);
        vibrateOn =preferences.getBoolean("VIBRATE",true);

        connectedNames=new ArrayList<>();
        chars=new ArrayList<>();
        connectedListAdapter =new ListAdapter(this,R.layout.connected_clients_list,connectedNames,chars);
        connectedDevicesList=new JazzyListView(this);
        connectedDevicesList.setAdapter(connectedListAdapter);
        connectedDevicesList.setTransitionEffect(JazzyHelper.CARDS);

        hosts=new ArrayList<>();
        hostListAdapter =new HostListAdapter(this,R.layout.host_list_layout,hosts);
        hostList=new JazzyListView(this);
        hostList.setTransitionEffect(JazzyHelper.CARDS);
        hostList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                NsdServiceInfo host= (NsdServiceInfo) hostListAdapter.getItem(i);
                createChoosePacmanDialog(false,host).show();
            }
        });
        hostList.setAdapter(hostListAdapter);

        initializeHelpAndSettings();

        initializeAllDialogs();

        sp.play(pacmanBeginning,1,1,1,0,1);


        Button start= (Button) findViewById(R.id.game_start);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGameDialog.show();
            }
        });

        Button settings= (Button) findViewById(R.id.settings);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingsDialog.show();
            }
        });

        Button help= (Button) findViewById(R.id.help);
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                helpDialog.show();
            }
        });
    }



    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        helper.tearDown();
        helper.stopDiscovery();
    }


    private void initializeAllDialogs() {
        waitingForGame=new MaterialDialog.Builder(this)
                .title("Waiting for game")
                .content("Please wait while other players join")
                .progress(true,0)
                .cancelable(false)
                .build();

        searchingGames=new MaterialDialog.Builder(this)
                .title("Searching Games")
                .customView(hostList,false)
                .cancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        helper.stopDiscovery();
                        hosts.clear();
                        hostListAdapter.notifyDataSetChanged();
                    }
                })
                .build();

        MaterialDialog.InputCallback hostNameCallback=new MaterialDialog.InputCallback() {
            @Override
            public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                hostName=input.toString();
                //hostWaiting.setIcon(getResources().getDrawable(getPacmanImageResource(selectedPacmanChars.get(0))));
                hostWaiting.show();
                int port=initiateServer();

                Log.e("Service registering: ","now "+port);
                if(port!=0){
                    helper.registerService(hostName,port,hostWaiting);

                }
                else {
                    Toast.makeText(getApplicationContext(),"Hosting Failed.Try Again.",Toast.LENGTH_LONG).show();
                    hostWaiting.dismiss();
                }
            }
        };

        enterHostName=new MaterialDialog.Builder(this)
                .title("Host Name")
                .content("Enter a hostname for your friends to find you.")
                .input("Host Name",null,false,hostNameCallback)
                .build();

        MaterialDialog.InputCallback nicknameCallback=new MaterialDialog.InputCallback() {
            @Override
            public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                nickName=input.toString();
                prefsEditor.putString("NICKNAME",nickName).apply();
                searchingGames.show();
            }
        };
        enterNickName=new MaterialDialog.Builder(this)
                .title("Nickname")
                .content("Enter a nickname for your PacMan")
                .input("NickName",null,false,nicknameCallback)
                .build();

        hostWaiting=new MaterialDialog.Builder(this)
                .title("Waiting for friends to join...")
                .customView(connectedDevicesList,false)
                .cancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        helper.tearDown();
                        connectedNames.clear();
                        connectedListAdapter.notifyDataSetChanged();
                    }
                })
                .positiveText("Start Game")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        if(!connectedNames.isEmpty()){
                            hostWaiting.dismiss();
                            startGame();
                        }
                        else {
                            Toast.makeText(getApplicationContext(),"Please Wait Until Players Join",Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .autoDismiss(false)
                .build();
        startGameDialog =new MaterialDialog.Builder(HomeScreen.this)
                .content("Start Game?")
                .positiveText("Host")
                .negativeText("Join")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        //enterHostName.show();
                        createChoosePacmanDialog(true,null).show();
                    }
                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                        if(nickName.equals("")){
                            enterNickName.show();
                        }
                        else {
                            searchingGames.show();
                        }
                        helper.discoverServices(hostListAdapter,hosts);

                    }
                })
                .build();

    }

    private void initializeHelpAndSettings() {

        View settingsDialogView=getLayoutInflater().inflate(R.layout.settings_dialog,null);
        CheckBox sound= (CheckBox) settingsDialogView.findViewById(R.id.settings_sound_checkbox);
        sound.setChecked(soundOn);
        sound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                prefsEditor.putBoolean("SOUND",b).apply();
            }
        });
        CheckBox vibrate= (CheckBox) settingsDialogView.findViewById(R.id.settings_vibrate_checkbox);
        vibrate.setChecked(vibrateOn);
        vibrate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                prefsEditor.putBoolean("VIBRATE",b).apply();
            }
        });
        settingsDialogView.findViewById(R.id.settings_change_nickname_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MaterialDialog.InputCallback nicknameCallback=new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                        nickName=input.toString();
                        prefsEditor.putString("NICKNAME",nickName).apply();
                        Toast.makeText(getApplicationContext(),"NickName changed to "+nickName,Toast.LENGTH_SHORT).show();
                    }
                };
                MaterialDialog enterNickName=new MaterialDialog.Builder(HomeScreen.this)
                        .title("Nickname")
                        .content("Enter a nickname for your PacMan")
                        .input("NickName",null,false,nicknameCallback)
                        .build();
                enterNickName.show();
            }
        });
        settingsDialog=new MaterialDialog.Builder(this)
                .title("Settings")
                .customView(settingsDialogView,true)
                .build();

        View helpDialogView=getLayoutInflater().inflate(R.layout.help_dialog_layout,null);
        helpDialog=new MaterialDialog.Builder(this)
                .title("Help")
                .customView(helpDialogView,true)
                .build();
    }



    private MaterialDialog createChoosePacmanDialog(boolean byHost,final NsdServiceInfo host) {

        View choosePacmanView=getLayoutInflater().inflate(R.layout.choose_pacman_layout,null);

        final MaterialDialog dialog=new MaterialDialog.Builder(this)
                .customView(choosePacmanView,false)
                .build();

        if(!byHost){  //pacman selection by clients
            choosePacmanView.findViewById(R.id.blinky_layout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    connectToHost(host,0);
                    dialog.dismiss();
                }
            });
            choosePacmanView.findViewById(R.id.inky_layout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    connectToHost(host, 1);
                    dialog.dismiss();
                }
            });
            choosePacmanView.findViewById(R.id.pinky_layout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    connectToHost(host, 2);
                    dialog.dismiss();
                }
            });
            choosePacmanView.findViewById(R.id.clyde_layout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    connectToHost(host, 3);
                    dialog.dismiss();
                }
            });
        }
        else {  //pacman selection by Host
            choosePacmanView.findViewById(R.id.blinky_layout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedPacmanChars.clear();
                    selectedPacmanChars.add(0);
                    avatar[0]=0;
                    dialog.dismiss();
                    enterHostName.show();
                }
            });
            choosePacmanView.findViewById(R.id.inky_layout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedPacmanChars.clear();
                    selectedPacmanChars.add(1);
                    avatar[0]=1;
                    dialog.dismiss();
                    enterHostName.show();
                }
            });
            choosePacmanView.findViewById(R.id.pinky_layout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedPacmanChars.clear();
                    selectedPacmanChars.add(2);
                    avatar[0]=2;
                    dialog.dismiss();
                    enterHostName.show();
                }
            });
            choosePacmanView.findViewById(R.id.clyde_layout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedPacmanChars.clear();
                    selectedPacmanChars.add(3);
                    avatar[0]=3;
                    dialog.dismiss();
                    enterHostName.show();
                }
            });
        }
        choosePacmanView.findViewById(R.id.close_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        return dialog;
    }

    private int initiateServer() {
        int port=0;
        try {
            welcomeSocket=new ServerSocket(0);
            port=welcomeSocket.getLocalPort();
            Log.e("Port",""+port);
            startAcceptThread(welcomeSocket);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return port;
    }

    private void connectToHost(final NsdServiceInfo host, final int pacmanCharacter) {

        /*
        * pacmanCharacter       PACMAN
        * *******************************
        *    0                  blinky
        *    1                  inky
        *    2                  pinky
        *    3                  clyde
        */

        Thread connect=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    clientSocket=new Socket(host.getHost(),host.getPort());
                    new DataOutputStream(clientSocket.getOutputStream()).writeInt(pacmanCharacter);   //sending pacmanCharacter for confirmation
                    String response=new DataInputStream(clientSocket.getInputStream()).readUTF();
                    if(response.equals("ACCEPTED")){
                        Log.e("Connected to","remote host");
                        clientId=new DataInputStream(clientSocket.getInputStream()).readInt();
                        avatar[clientId]=pacmanCharacter;
                        Log.e("received id"," "+clientId);
                        helper.stopDiscovery();
                        hosts.clear();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hostListAdapter.notifyDataSetChanged();
                                searchingGames.dismiss();
                                waitForStartGame();
                                waitingForGame.show();
                                Toast.makeText(getApplicationContext(),"You are Client : "+clientId,Toast.LENGTH_LONG).show();
                            }
                        });
                        new DataOutputStream(clientSocket.getOutputStream()).writeUTF(nickName);
                        Log.e("Connected To",host.getServiceName());
                    }
                    else if(response.equals("PACMAN_ALREADY_SELECTED")){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(),"Pacman Already Selected by another player, Choose a different Pacman",Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(),"Couldn't connect to host :(",Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("Conn","Exception");
                            Toast.makeText(getApplicationContext(),"Couldn't connect to host :(",Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
        connect.start();
    }

    private void startAcceptThread(final ServerSocket welcomeSoccket) {
        clients.clear();
        connectedNames.clear();
        Thread acceptConn=new Thread(new Runnable() {
            @Override
            public void run() {
                int i=0;
                while(true){
                    try {
                        Socket temp=welcomeSoccket.accept();
                        int pacmanSelectedByClient= new DataInputStream(temp.getInputStream()).readInt();
                        if(!selectedPacmanChars.contains(pacmanSelectedByClient)){
                            clients.add(temp);
                            selectedPacmanChars.add(pacmanSelectedByClient);
                            new DataOutputStream(temp.getOutputStream()).writeUTF("ACCEPTED");
                            Log.e("Accepted","Client "+i+" : "+clients.get(i).getInetAddress());
                            new DataOutputStream(clients.get(i).getOutputStream()).writeInt(i+1);  //sending client ID
                            avatar[i+1]=pacmanSelectedByClient;
                            String nickname=new DataInputStream(clients.get(i).getInputStream()).readUTF();
                            connectedNames.add(nickname);
                            chars.add(pacmanSelectedByClient);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    hostWaiting.setCancelable(false);  //cannot cancel dialog anymore...
                                    connectedListAdapter.notifyDataSetChanged();
                                }
                            });
                            i++;
                        }
                        else {
                            //pacman char already selected
                            new DataOutputStream(temp.getOutputStream()).writeUTF("PACMAN_ALREADY_SELECTED");
                        }
                        if(i==3){
                            break;
                        }
                    } catch (IOException e) {

                        Log.e("IO exception",e.toString());
                        e.printStackTrace();
                    }
                }
                connFull=true;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),"Maximum ghosts joined",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
        acceptConn.start();
    }

    private void startGame(){
        helper.tearDown();
        connectedNames.clear();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                connectedListAdapter.notifyDataSetChanged();
            }
        });
        Thread sendStartMsg=new Thread(new Runnable() {
            @Override
            public void run() {
                for(Socket client:clients){
                    try {
                        new DataOutputStream(client.getOutputStream()).writeUTF("GAME_ON");
                        new DataOutputStream(client.getOutputStream()).writeInt(clients.size());    //sending no.of clients to clients
                        for(int i=0;i<selectedPacmanChars.size();i++){
                            new DataOutputStream(client.getOutputStream()).writeInt(selectedPacmanChars.get(i));  //sending pacman character of each client to every client
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent=new Intent(HomeScreen.this,MainActivity.class);
                        intent.putExtra("MODE","HOST");
                        intent.putExtra("AVATAR",avatar);
                        startActivity(intent);
                    }
                });
            }
        });
        sendStartMsg.start();
    }

    private void waitForStartGame(){
        Thread wait=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String msg=new DataInputStream(clientSocket.getInputStream()).readUTF();  //receive GAME_ON
                    final int clientNum=new DataInputStream(clientSocket.getInputStream()).readInt();  //receive no.of clients
                    for(int i=0;i<clientNum;i++){
                        avatar[i]=new DataInputStream(clientSocket.getInputStream()).readInt();   //receive avatars of host and other clients
                    }
                    if(msg.equals("GAME_ON")){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                waitingForGame.dismiss();
                                Intent intent=new Intent(HomeScreen.this,MainActivity.class);
                                intent.putExtra("MODE","CLIENT");
                                intent.putExtra("CLIENT_NUM",clientNum);
                                intent.putExtra("ID",clientId);
                                intent.putExtra("AVATARS",avatar);
                                startActivity(intent);
                            }
                        });
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        wait.start();

    }

    public void acheivementsClick(View view) {

        MaterialDialog dialog=new MaterialDialog.Builder(this)
                .title("Acheivements")
                .content("Last Score : "+preferences.getInt("LAST_SCORE",0)+"\n\nTotal Score : "+preferences.getInt("TOTAL_SCORE",0))
                .build();

        dialog.show();
    }
}
