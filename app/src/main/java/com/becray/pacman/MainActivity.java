package com.becray.pacman;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Semaphore;

public class MainActivity extends AppCompatActivity {

    static int SPEED=400;
    static int ROWS =25, COLUMNS =18;
    static int maxHeight;
    static int maxWidth;
    static int gridHeight;
    static int gridWidth;
    static SwipeDetector.SwipeTypeEnum[] pacDirection;
    static SwipeDetector.SwipeTypeEnum[] ghostDirections;
    static MaterialDialog lostDialog,wonDialog;
    static ImageView[][] grid=new ImageView[COLUMNS][ROWS];
    static boolean[] caught;
    static int NO_OF_GHOSTS=4;
    static int NO_OF_CLIENTS;
    static int ID =-1;   //if this device is a client,, this will be its ID  if host,, id=0;
    static int[] ghostX,ghostY;   // arrays to store X & Y positions of all ghosts.
    static int[] pacX,pacY;
    static boolean STOP_ALL_THREADS=false;
    static int[] scores;

    static int pacData=722626,ghostData=44678, abortData =22678, energizerData=363749;

    Semaphore mutex;
    static Semaphore self;
    boolean isHost;
    MaterialDialog.Builder builder;
    MaterialDialog waitUntilGameEnds;
    Map m=new Map();
    int[] avatars;
    static PacMan[] pacmans;
    Ghost[] ghosts;
    ImageView[] ghostViews;
    ImageView[] pacmanViews;

    static ArrayList<Socket> clients;
    static Socket clientSocket;

    RelativeLayout objectSquare,gameSquare,controlLayout,baseLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //GO FULL SCREEN
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //KEEP SCREEN ON EVEN THOUGH USER IS IDLE
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();
        mutex=new Semaphore(1);
        self=new Semaphore(1);
        STOP_ALL_THREADS=false;
        gameSquare= (RelativeLayout) findViewById(R.id.game_sqauare);
        objectSquare= (RelativeLayout) findViewById(R.id.object_sqauare);
        controlLayout= (RelativeLayout) findViewById(R.id.control_layout);
        baseLayout= (RelativeLayout) findViewById(R.id.base_layout);


        builder=new MaterialDialog.Builder(this);

        //now client or host does its own stuff
        Intent intent=getIntent();
        Bundle extras=intent.getExtras();

        if(extras.getString("MODE").equals("HOST")){
            //Host stuff
            Log.e("Mode",extras.getString("MODE"));
            isHost=true;
            clients= HomeScreen.clients;
            NO_OF_CLIENTS=clients.size();
            ID=0;
            avatars=HomeScreen.avatar;
            scores=new int[NO_OF_CLIENTS+1];
            Arrays.fill(scores,0);
        }
        else if(extras.getString("MODE").equals("CLIENT")){
            //client stuff
            Log.e("Mode",extras.getString("MODE"));
            isHost=false;
            clientSocket=HomeScreen.clientSocket;
            ID =extras.getInt("ID");
            NO_OF_CLIENTS=extras.getInt("CLIENT_NUM");
            avatars=extras.getIntArray("AVATARS");
            scores=new int[NO_OF_CLIENTS+1];
            Arrays.fill(scores,0);
        }

        ghostDirections=new SwipeDetector.SwipeTypeEnum[NO_OF_GHOSTS];
        ghostX=new int[NO_OF_GHOSTS];
        ghostY=new int[NO_OF_GHOSTS];

        pacX=new int[NO_OF_CLIENTS+1];
        pacY=new int[NO_OF_CLIENTS+1];

        caught=new boolean[NO_OF_CLIENTS+1];
        Arrays.fill(caught,false);

        //set swipe detector to detect swipes and map it to this device's pacman's movement.
        new SwipeDetector(controlLayout).setOnSwipeListener(new SwipeDetector.onSwipeEvent() {
            @Override
            public void SwipeEventDetected(View v, SwipeDetector.SwipeTypeEnum SwipeType) {
                boolean flag=true;
                switch (SwipeType){
                    case LEFT_TO_RIGHT:{
                        if(pacX[ID]+1> COLUMNS -1||m.matrix[pacX[ID]+1][pacY[ID]]==Map.Object.WALL){flag=false;Log.e("Pac Swipe ignored",SwipeType.toString());}break;
                    }
                    case RIGHT_TO_LEFT:{
                        if(pacX[ID]-1<0||m.matrix[pacX[ID]-1][pacY[ID]]==Map.Object.WALL){flag=false;Log.e("Pac Swipe ignored",SwipeType.toString());}break;
                    }
                    case TOP_TO_BOTTOM:{
                        if(pacY[ID]+1> ROWS -1||m.matrix[pacX[ID]][pacY[ID]+1]==Map.Object.WALL){flag=false;Log.e("Pac Swipe ignored",SwipeType.toString());}break;
                    }
                    case BOTTOM_TO_TOP:{
                        if(pacY[ID]-1<0||m.matrix[pacX[ID]][pacY[ID]-1]==Map.Object.WALL){flag=false;Log.e("Pac Swipe ignored",SwipeType.toString());}break;
                    }
                }

                if(flag){
                    pacDirection[ID] = SwipeType;
                }


            }
        });


        lostDialog=builder
                .title("Game Over")
                .cancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        if(isHost) {
                            if(hasGameEnded()){
                                finish();
                            }
                            else {
                                waitUntilGameEnds=new MaterialDialog.Builder(MainActivity.this)
                                        .progress(true,0)
                                        .title("Please Wait.")
                                        .content("You are the host. Please wait until the game ends")
                                        .cancelListener(new DialogInterface.OnCancelListener() {
                                            @Override
                                            public void onCancel(DialogInterface dialogInterface) {
                                                if(hasGameEnded()){
                                                    finish();
                                                }
                                                else{
                                                    waitUntilGameEnds.show();
                                                }
                                            }
                                        })
                                        .build();
                                waitUntilGameEnds.show();
                            }
                        }
                        else {
                            //if client
                            finish();
                        }
                    }
                })
                .build();
        builder=new MaterialDialog.Builder(this);
        wonDialog=builder
                .title("PacMan :)")
                .content("YaY!!.. We Won")
                .cancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        finish();
                    }
                })
                .build();




        m.loadMap1();

        setScreen();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        STOP_ALL_THREADS=true;   //    :-)
        pacX[ID]=1;
        pacY[ID]=1;
        Arrays.fill(pacDirection, SwipeDetector.SwipeTypeEnum.TOP_TO_BOTTOM);
        Arrays.fill(ghostDirections, SwipeDetector.SwipeTypeEnum.LEFT_TO_RIGHT);
        Arrays.fill(caught,false);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if(!caught[ID]){
            MaterialDialog areYouSure=new MaterialDialog.Builder(this)
                    .title("Quit Game?")
                    .content("Are you sure you want to quit the game?")
                    .positiveText("Yes")
                    .negativeText("No")
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            super.onPositive(dialog);
                            if(ID==0){  //if host
                                stopGame();
                            }
                            finish();
                        }
                    })
                    .build();
            areYouSure.show();
        }
    }

    private void UpdateMap(final int i, final int j, final Map.Object object){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (object){
                    case EMPTY:grid[i][j].setImageDrawable(null);break;
                    case ENERGIZER:grid[i][j].setImageResource(R.drawable.energy);
                }
            }
        });
    }

    private boolean hasGameEnded(){
        boolean gameEnd=true;
        int i=0;
        for (boolean aCaught : caught) {
            if (!aCaught) {
                Log.e("ID "+i,"Not caught");
                gameEnd = false;
                break;
            }
            i++;
        }

        return gameEnd;
    }


    //start accepting client changes...
    private void startAcceptingClientResponses(final Socket fromClient) {
        Thread acceptClientResponses=new Thread(new Runnable() {
            @Override
            public void run() {
                while(!STOP_ALL_THREADS){
                    try {
                        int from=new DataInputStream(fromClient.getInputStream()).readInt();    //read ID of sending client
                        int x=new DataInputStream(fromClient.getInputStream()).readInt();  //read X position of client
                        int y=new DataInputStream(fromClient.getInputStream()).readInt();  //read Y position of client
                        SwipeDetector.SwipeTypeEnum direction = SwipeDetector.getDirectionEnum(
                                new DataInputStream(fromClient.getInputStream()).readInt());  //read direction of sending client's pacman


                        pacDirection[from]=direction;
                        pacX[from]=x;
                        pacY[from]=y;
                        pacmans[from].movePacman(x,y,pacDirection[from]);   //move the pacman in host device

                        updateAllClientsPacmanPosition(from, direction);  //send the change happened in pacman position to all clients except the one that send..
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        acceptClientResponses.start();
    }
    private void stopGame(){
        Thread stopGame=new Thread(new Runnable() {
            @Override
            public void run() {
                for(Socket socket:clients){
                    try {
                        //BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                        //bw.write("ABORT");
                        //BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                        //PrintWriter out = new PrintWriter(bw, true);
                        //out.write(abortData);
                        new DataOutputStream(socket.getOutputStream()).writeInt(abortData);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        stopGame.start();
    }

    //function to update all client's pacman position except the one that made the last change.  id -> ID of client that caused the change
    //id=0 if callee is host
    public static void updateAllClientsPacmanPosition(final int id, final SwipeDetector.SwipeTypeEnum dir){
        Thread updateThread=new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i=1;i<NO_OF_CLIENTS+1;i++){
                    if(i!=id){
                        try {
                            self.acquire();
                            //Log.e("sending pac positions","now");

                            //BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(clients.get(i-1).getOutputStream()));
                            //PrintWriter out = new PrintWriter(bw, true);
                            //out.write(pacData);

                            new DataOutputStream(clients.get(i-1).getOutputStream()).writeInt(pacData);    //prepare clients to receive pacman positions
                            new DataOutputStream(clients.get(i-1).getOutputStream()).writeInt(id);  //send id of pacman moved
                            new DataOutputStream(clients.get(i-1).getOutputStream()).writeInt(pacX[id]);  //send X position of pacman
                            new DataOutputStream(clients.get(i-1).getOutputStream()).writeInt(pacY[id]);  //send Y position of pacman
                            new DataOutputStream(clients.get(i-1).getOutputStream()).writeInt(
                                    SwipeDetector.getDirectionInt(pacDirection[id])); //send current direction of pacman
                            self.release();
                        }
                        catch (IOException e) {
                            Log.e("sending pac positions","exception IO  "+e.getMessage());
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            Log.e("sending pac positions","exception Interr  "+e.getMessage());
                            e.printStackTrace();
                        }
                    }

                }
            }
        });
        updateThread.start();
    }


    //function to send position of a ghost to all clients
    public static void updateAllClientsGhostPositions(final int ghostID) {
        Thread updateThread=new Thread(new Runnable() {
            @Override
            public void run() {
                for(Socket client:clients){
                    try {
                        self.acquire();
                        //Log.e("sending ghost positions","now");
                        //BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
                        //PrintWriter out = new PrintWriter(bw, true);
                        //out.write(ghostData);

                        new DataOutputStream(client.getOutputStream()).writeInt(ghostData);   //prepare clients to receive ghost positions
                        new DataOutputStream(client.getOutputStream()).writeInt(ghostID);   //send ID of ghost
                        new DataOutputStream(client.getOutputStream()).writeInt(ghostX[ghostID]);  //send X position of ghost
                        new DataOutputStream(client.getOutputStream()).writeInt(ghostY[ghostID]);  //send Y position of ghost
                        new DataOutputStream(client.getOutputStream()).writeInt(
                                SwipeDetector.getDirectionInt(ghostDirections[ghostID]));  //send current direction of ghost
                        self.release();
                    }
                    catch (IOException e) {
                        Log.e("sending ghost positions","exception IO  "+e.getMessage());
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        Log.e("sending ghost positions","exception Interr  "+e.getMessage());
                        e.printStackTrace();
                    }
                }
            }
        });
        updateThread.start();
    }

    //function to start accept host messages.
    private void startAcceptingHostMessages(){
        Thread acceptHostMessages=new Thread(new Runnable() {
            @Override
            public void run() {
                while(!STOP_ALL_THREADS){
                        try {
                            //Log.e("SAHM","RUN in while");
                            int data;
                            //BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                            //data=br.readLine();
                            data=new DataInputStream(clientSocket.getInputStream()).readInt();

                            if(data==(ghostData)){
                                //update position of a ghost in client
                                int ghostID = new DataInputStream(clientSocket.getInputStream()).readInt();  //receive id ghost
                                int x=new DataInputStream(clientSocket.getInputStream()).readInt();  //receive X position of ghost
                                int y=new DataInputStream(clientSocket.getInputStream()).readInt();  //receive Y position of ghost
                                int direction = new DataInputStream(clientSocket.getInputStream()).readInt();   //receive current direction of ghost

                                //Log.e("Ghost "+ghostID,x+"  "+y+"  "+direction);
                                ghostDirections[ghostID] = SwipeDetector.getDirectionEnum(direction);
                                ghostX[ghostID]=x;
                                ghostY[ghostID]=y;

                                ghosts[ghostID].moveGhost(x,y,ghostDirections[ghostID]);
                            }
                            else if(data==(pacData)){
                                //update position of pacman in client device
                                int ID=new DataInputStream(clientSocket.getInputStream()).readInt();  //receive ID of client, whose pacman moved
                                int x=new DataInputStream(clientSocket.getInputStream()).readInt();  //receive X position of pacman
                                int y=new DataInputStream(clientSocket.getInputStream()).readInt();  //receive Y position of pacman
                                int pacDir=new DataInputStream(clientSocket.getInputStream()).readInt();  //receive current direction of pacman

                                //Log.e("Pacman "+ID,x+"  "+y+"  "+pacDir);
                                MainActivity.pacDirection[ID] =SwipeDetector.getDirectionEnum(pacDir);

                                pacmans[ID].movePacman(x,y,MainActivity.pacDirection[ID]);   //make the pacman in this device move
                            }
                            else if(data==(abortData)){
                                MainActivity.STOP_ALL_THREADS=true;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        MaterialDialog gameEndDialog=new MaterialDialog.Builder(MainActivity.this)
                                                .title("Ooops!!!")
                                                .content("The game has been stopped by the host!!!")
                                                .cancelListener(new DialogInterface.OnCancelListener() {
                                                    @Override
                                                    public void onCancel(DialogInterface dialogInterface) {
                                                        finish();
                                                    }
                                                })
                                                .build();
                                        gameEndDialog.show();
                                    }
                                });
                            }
                        } catch (IOException e) {
                            Log.e("SAHM","Exception "+e.getMessage());
                            e.printStackTrace();
                        }

                }
                }
        });

        acceptHostMessages.start();
    }

    //send a change of position in a client to host
    public static void informHost(final SwipeDetector.SwipeTypeEnum direction){
        Thread inform=new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    //self.acquire();
                    new DataOutputStream(clientSocket.getOutputStream()).writeInt(ID);   //send ID of client
                    new DataOutputStream(clientSocket.getOutputStream()).writeInt(pacX[ID]);  //send X position
                    new DataOutputStream(clientSocket.getOutputStream()).writeInt(pacY[ID]);  //send Y position
                    new DataOutputStream(clientSocket.getOutputStream()).writeInt(SwipeDetector.getDirectionInt(direction));  //send direction it moved
                    //self.release();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        inform.start();
    }

    //measure the screen size and calculate size of each cell in the grid
    //also calls functions initCharacters(),LoadCharacters() and initMap()
    private void setScreen(){
        ViewTreeObserver observer=objectSquare.getViewTreeObserver();
        if(observer.isAlive()){
            observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    maxHeight=objectSquare.getMeasuredHeight();
                    maxWidth=objectSquare.getMeasuredWidth();
                    gridHeight=maxHeight/ ROWS;
                    gridWidth=maxWidth/ COLUMNS;
                    objectSquare.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    Log.e("Height/Width",objectSquare.getMeasuredHeight()+"   "+objectSquare.getMeasuredWidth()+"    "+gridHeight+"    "+gridWidth);

                    RelativeLayout.LayoutParams temp= (RelativeLayout.LayoutParams) objectSquare.getLayoutParams();

                    temp.height= ROWS *gridHeight;
                    temp.width= COLUMNS *gridWidth;

                    objectSquare.setLayoutParams(temp);
                    gameSquare.setLayoutParams(temp);


                    initCharacters();

                    InitMap();

                    LoadCharacters();

                    if(isHost){

                        for(Socket s:clients){
                            startAcceptingClientResponses(s);
                        }
                    }
                    else {
                        startAcceptingHostMessages();
                    }
                }
            });
        }
    }

    //initialize the characters and their image views.
    private void initCharacters(){
        ghosts=new Ghost[NO_OF_GHOSTS];
        ghostViews=new ImageView[NO_OF_GHOSTS];
        pacmans=new PacMan[NO_OF_CLIENTS+1];
        pacmanViews =new ImageView[NO_OF_CLIENTS+1];

        for(int i=0;i<NO_OF_CLIENTS+1;i++){
            pacmanViews[i]=new ImageView(MainActivity.this);
        }

        for(int i=0;i<NO_OF_GHOSTS;i++){
            ghostViews[i]=new ImageView(MainActivity.this);
        }

        ghostDirections=new SwipeDetector.SwipeTypeEnum[NO_OF_GHOSTS];
        for(int i=0;i<NO_OF_GHOSTS;i++){
            ghostDirections[i]= SwipeDetector.SwipeTypeEnum.LEFT_TO_RIGHT;
        }

        pacDirection=new SwipeDetector.SwipeTypeEnum[NO_OF_CLIENTS+1];
        for(int i=0;i<NO_OF_CLIENTS+1;i++){
            pacDirection[i]= SwipeDetector.SwipeTypeEnum.TOP_TO_BOTTOM;
        }

        for(int i=0;i<NO_OF_CLIENTS+1;i++){
            pacmans[i]=new PacMan(MainActivity.this,i,HomeScreen.avatar[i], pacmanViews[i],m,mutex);
        }

        for(int i=0;i<NO_OF_GHOSTS;i++){
            ghosts[i]=new Ghost(MainActivity.this,i,ghostViews[i],m,mutex,10,23);
        }

    }

    //initialize the map objects like food,mega food, walls
    private void InitMap(){
        for (int i = 0; i< COLUMNS; i++){
            for(int j = 0; j< ROWS; j++){
                ImageView iv=new ImageView(MainActivity.this);
                //iv.setBackgroundDrawable(drawable);

                RelativeLayout.LayoutParams params=new RelativeLayout.LayoutParams(gridWidth,gridHeight);
                params.setMargins(i*gridWidth,j*gridHeight,0,0);
                iv.setLayoutParams(params);
                iv.setScaleType(ImageView.ScaleType.FIT_XY);
                if(m.matrix[i][j]==Map.Object.WALL)
                    iv.setImageResource(R.drawable.wall_grey);
                else if(m.matrix[i][j]== Map.Object.FOOD)
                    iv.setImageResource(R.drawable.food_small);
                else if(m.matrix[i][j]== Map.Object.ENERGIZER)
                    iv.setImageResource(R.drawable.mega_food);
                grid[i][j]=iv;
                objectSquare.addView(iv);
            }
        }

        Log.e("Map","initialized");
    }

    //Loads the characters into screen
    private void LoadCharacters(){
        RelativeLayout.LayoutParams par=new RelativeLayout.LayoutParams(gridWidth,gridHeight);
        for(int i=0;i<NO_OF_CLIENTS+1;i++){
            par.setMargins(gridWidth,gridHeight,0,0);
            pacmanViews[i].setLayoutParams(par);
            gameSquare.addView(pacmanViews[i]);
        }

        RelativeLayout.LayoutParams ghostPar=new RelativeLayout.LayoutParams(gridWidth,gridHeight);
        for(int i=0;i<NO_OF_GHOSTS;i++){
            ghostPar.setMargins(10*gridWidth,23*gridHeight,0,0);
            ghostViews[i].setLayoutParams(ghostPar);
            ghostViews[i].setImageResource(R.drawable.ghost_red_smooth);
            gameSquare.addView(ghostViews[i]);
        }
        //m.ghostMatrix[10][23]=true;

        Log.e("Characters","Loaded");
        Toast.makeText(getApplicationContext(),"Swipe the screen to move PacMan",Toast.LENGTH_LONG).show();

        //all devices start movement of their respective pacman.
        pacmans[ID].movePacmanThread();

        //only host starts the ghost threads. ghosts in the clients moves according to movement of ghosts of host.
        if(isHost){
            Log.e("host starting threads",""+ ID);
            for (int i = 0; i < NO_OF_GHOSTS; i++) {
                /*
                * Only Host Simulates ghost movement...
                * clients imitate host by receiving it..
                */
                ghosts[i].randomGhost(ghostViews[i]);
            }
        }

    }

}
